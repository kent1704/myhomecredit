/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobile.Runner;

/**
 *
 * @author kentvanlim
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//import com.ameera.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import com.mobile.Locator.ElementLocator;
import static io.appium.java_client.touch.offset.ElementOption.element;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebElement;


/**
 *
 * @author Kent Vanlim
 */
public class Runner {
    
AppiumDriver<MobileElement> driver;


    public static void main(String[] args) {
        
        //Set the Desired Capabilities
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", "emulator-5554");
        caps.setCapability("platformName", "Android");
        caps.setCapability("appPackage", "id.co.myhomecredit");
        caps.setCapability("appActivity", "id.co.homecredit.v2.launcherpage.SplashScreenActivity");
        caps.setCapability("noReset", "true");

        AppiumDriver<MobileElement> driver = null;
        //Instantiate Appium Driver
        try {
            driver = new AndroidDriver<MobileElement>(
                    new URL("http://127.0.0.1:4723/wd/hub"), caps);

        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        }
        try {
            testDaftar(driver);

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }
    

    public static void testDaftar(AppiumDriver<MobileElement> driver){
      ElementLocator el = new ElementLocator();
            el.Action(driver);
            el.swipeImages(driver);
            el.klikRegister(driver);
       //     el.swipePersyaratan2(driver);
            el.swipePersyaratan(driver);
            //klikAgree(driver);
            el.isiBiodata(driver);
            el.konfirmasi(driver);
            el.backToHome(driver);
    }
  
}
