/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobile.Locator;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

/**
 *
 * @author kentvanlim
 */
public class ElementLocator {

    public void isiBiodata(AppiumDriver<MobileElement> driver) {
        MobileElement pilihButtonTanggal = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[1]/android.widget.Spinner[1]");
        pilihButtonTanggal.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement pilihTanggal = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[10]");
        pilihTanggal.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement pilihButtonBulan = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[1]/android.widget.Spinner[2]");
        pilihButtonBulan.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement pilihBulan = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[3]");
        pilihBulan.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement pilihButtonTahun = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[1]/android.widget.Spinner[3]");
        pilihButtonTahun.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement pilihTahun = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]");
        pilihTahun.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement klikButtonHp = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[2]/android.view.View/android.widget.EditText");
        klikButtonHp.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement isiNoHp = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[2]/android.view.View/android.widget.EditText");
        isiNoHp.sendKeys("82114192022");
        sleepBeforeAnotherExecutions(3000);
        MobileElement isiPin = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[3]/android.view.View/android.widget.EditText");
        isiPin.sendKeys("170497");
        sleepBeforeAnotherExecutions(3000);
        MobileElement konfirmasiPin = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[4]/android.view.View/android.widget.EditText");
        konfirmasiPin.sendKeys("170497");
        sleepBeforeAnotherExecutions(3000);

    }

    public void sleepBeforeAnotherExecutions(long milis) {
        try {
            Thread.sleep(milis);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public void Action(AppiumDriver<MobileElement> driver) {
        sleepBeforeAnotherExecutions(12000);
        new TouchAction(driver).tap(PointOption.point(532, 692)).perform();
        sleepBeforeAnotherExecutions(3000);
//       new TouchAction(driver).press(PointOption.point(732, 1024)).moveTo(PointOption.point(135, 1086)).release().perform();
//       sleepBeforeAnotherExecutions(3000);
//       new TouchAction(driver).press(PointOption.point(732, 1024)).moveTo(PointOption.point(135, 1086)).release().perform();
//       sleepBeforeAnotherExecutions(3000);
//       new TouchAction(driver).press(PointOption.point(740, 1024)).moveTo(PointOption.point(135, 1086)).release().perform();
//       sleepBeforeAnotherExecutions(3000);

    }

    public void swipeImages(AppiumDriver<MobileElement> driver) {
        for (int i = 0; i < 3; i++) {
            new TouchAction(driver).press(PointOption.point(898, 1024)).moveTo(PointOption.point(167, 1086)).release().perform();
            sleepBeforeAnotherExecutions(3000);
        }

    }

    public void swipePersyaratan(AppiumDriver<MobileElement> driver) {
        //    new TouchAction(driver).press(PointOption.point(105, 1447)).moveTo(PointOption.point(118, 818)).release().perform();  
        new TouchAction(driver).press(PointOption.point(105, 1447)).moveTo(PointOption.point(118, 818)).release().perform();

        boolean isFoundElement;
        MobileElement element = (MobileElement) driver.
                findElementById("id.co.myhomecredit:id/agree_button");

        //    isFoundElement = element.isEnabled();
//        while (isFoundElement == false) {
//            
//            new TouchAction(driver).press(PointOption.point(105, 1447)).moveTo(PointOption.point(118, 818)).release().perform();      
//         
//            isFoundElement = element.isEnabled();
//        }
//        element.click();
//        sleepBeforeAnotherExecutions(5000);
        do {

            new TouchAction(driver).press(PointOption.point(105, 1447)).moveTo(PointOption.point(118, 818)).release().perform();

            isFoundElement = element.isEnabled();
        } while (isFoundElement == false);
        element.click();
        sleepBeforeAnotherExecutions(10000);
    }

    public void swipePersyaratan2(AppiumDriver<MobileElement> driver) {
        for (int i = 0; i < 35; i++) {
            new TouchAction(driver).press(PointOption.point(105, 1447)).moveTo(PointOption.point(118, 818)).release().perform();
            sleepBeforeAnotherExecutions(3000);
        }
        MobileElement element = (MobileElement) driver.
                findElementById("id.co.myhomecredit:id/agree_button");
        element.click();

    }

    public void klikRegister(AppiumDriver<MobileElement> driver) {
        MobileElement buttonRegister = (MobileElement) driver.findElementById("id.co.myhomecredit:id/button_register");
        buttonRegister.click();
        sleepBeforeAnotherExecutions(3000);

    }

    public void klikAgree(AppiumDriver<MobileElement> driver) {
        MobileElement buttonSetuju = (MobileElement) driver.findElementById("id.co.myhomecredit:id/agree_button");
        buttonSetuju.click();
        sleepBeforeAnotherExecutions(3000);

    }

    public void konfirmasi(AppiumDriver<MobileElement> driver) {
        MobileElement submit = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[5]/android.widget.Button");
        submit.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement submit2 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[4]/android.view.View/android.view.View[6]/android.widget.Button");
        submit2.click();
        sleepBeforeAnotherExecutions(3000);
        MobileElement submit3 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[4]/android.view.View/android.widget.Button");
        submit3.click();
        sleepBeforeAnotherExecutions(3000);

    }

    public void backToHome(AppiumDriver<MobileElement> driver) {
        MobileElement backButton = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[1]/android.widget.Image[1]");
        backButton.click();

    }
}
